
// масив картинок
let images = document.querySelectorAll('.image-to-show');
console.log(images);
let i = 0;
let timerId;

function nextImg() {
    if (i === images.length - 1) {
        // ховаємо останне зображення
        images[i].style.display = 'none';
        // як доходим до кінця масиву обнуляємо лічильник
        i = 0;
        // показуємо пер
        images[0].style.display = 'block'; 
    } else {
        images[i].style.display = 'none';
        images[i + 1].style.display = 'block';
        i++;
    }
}
timerId = setInterval(nextImg, 3000);

let pause = document.querySelector('.pause');
let play = document.querySelector('.play');

function pauseImg() {
    play.disabled = false;
    pause.disabled = true;
    clearInterval(timerId);
};

function playImg() {
    play.disabled = true;
    pause.disabled = false;
    timerId = setInterval(nextImg, 3000);
};

pause.addEventListener('click', pauseImg);
play.addEventListener('click', playImg);
play.disabled = true;


// https://www.youtube.com/watch?v=pVMK9d957Cw

// Чтобы «остановить» значение пиши его в переменную, которая позволит начать отсчет не с самого начала. Чтобы можно было восстанавливать, пиши в другую переменную свой интервал и снуляй его при тех событиях, когда тебе надо.
// https://snipp.ru/jquery/pause-settimeout#
// https://stackoverflow.com/questions/24724852/pause-and-resume-setinterval
// https://gist.github.com/ncou/3a0a1f89c8e22416d0d607f621a948a9